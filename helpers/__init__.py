def get_user_keycloak(username):
    pass


def generic_filter_by(cls, dict_filter):
    query = cls.query
    for attr, value in dict_filter.iteritems():
        if not hasattr(cls, str(attr)):
            continue
        query = query.filter(getattr(cls, attr)==value)

    return query.all()


from marshmallow import Schema, fields
from . import db, BaseModel


class UserModel(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(40), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(80), nullable=False)
    bl_active = db.Column(db.Boolean(), default=1, nullable=False)


class UserSchema(Schema):
    id = fields.Integer(dump_only=True, data_key="id")
    username = fields.String(required=True)
    email = fields.Email(required=True)
    password = fields.String(required=True)
    bl_active = fields.Boolean(required=False)
    role = fields.Integer(required=True)

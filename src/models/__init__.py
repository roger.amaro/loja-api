from flask_sqlalchemy import SQLAlchemy

from datetime import datetime

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True

    dh_inicio_registro = db.Column(db.DateTime(6), default=db.func.current_timestamp(), nullable=False)
    dh_ultimo_update = db.Column(db.DateTime(6), onupdate=db.func.current_timestamp(), nullable=False)
    dh_fim_registro = db.Column(db.DateTime(6), nullable=True)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        self.dh_fim_registro = datetime.utcnow()
        db.session.commit()

    def update(self, new_obj):
        for key, item in new_obj.items():
            setattr(self, key, item)
        db.session.commit()


from .UserModel import UserModel, UserSchema

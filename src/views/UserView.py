from flask_restful import Resource, reqparse
from src.models import UserSchema, UserModel
from helpers import generic_filter_by


class UserView(Resource):
    model = UserModel
    schema = UserSchema

    def post(self):
        parser = reqparse.RequestParser()
        json_new_user = parser.parse_args()
        self.schema().load(json_new_user)

    def get(self):
        pass

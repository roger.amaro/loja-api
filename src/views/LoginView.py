from flask_restful import Resource, abort, reqparse
from flask import jsonify, request
import os
import requests


class LoginView(Resource):

    def post(self):

        body = request.get_json()
        for field in ['username', 'password']:
            if field not in body:
                return abort(400, msg=f"Field {field} is missing!")
        data = dict(grant_type='password', client_id=os.getenv('CLIENT_ID'),
                    username=body['username'], password=body['password'])
        url = ''.join([
            os.getenv('KEYCLOAK_URI'),
            'auth/realms/',
            os.getenv('REALM'),
            '/protocol/openid-connect/token'
        ])
        response = requests.post(url, data=data, verify=False)
        if response.status_code > 200:
            message = "Error en username/password"
            return abort(400, msg=message)

        return response.json(), 200

from flask import Flask
from flask_jwt_simple import JWTManager
from flask_migrate import Migrate
from flask_restful import Api

from config import app_config
from src.models import db
from src.views import LoginView


def create_app(env_name):
    config = app_config[env_name]
    app = Flask(__name__)
    app.secret_key = config.SECRET
    app.config.from_object(config)
    app.config.from_pyfile('config.py')
    app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db.init_app(app)
    Migrate(app, db)
    api = Api(app)
    jwt = JWTManager(app)

    api.add_resource(LoginView, '/login')

    return app
